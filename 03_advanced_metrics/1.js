import { Counter, Trend, Rate } from 'k6/metrics';

import http from 'k6/http';

const Auth_OK_Counter = new Counter('Auth_OK_Counter');

const Auth_NOK_Counter = new Counter('Auth_NOK_Counter');


export const options = {
  vus: 1,
  duration: '10s',
  thresholds: {
    'Auth_NOK_Counter': ['count < 10', ],
  },
};

export default function () {
  const auth_resp = http.post('https://test-api.k6.io/auth/token/login/', {
    username: 'test-user',
    password: 'supersecure',
  });

  if (auth_resp.status == 200) {
    Auth_OK_Counter.add(1);
  } else
  {
    Auth_NOK_Counter.add(1);
  }

}
