import { Counter, Trend, Rate } from 'k6/metrics';
import { check } from 'k6';

import http from 'k6/http';

const Auth_OK_Counter = new Counter('Auth_OK_Counter');
const Auth_OK_Trend = new Trend('Auth_OK_Trend');
const Auth_OK_Rate = new Rate('Auth_OK_Rate');

const Auth_NOK_Counter = new Counter('Auth_NOK_Counter');
const Auth_NOK_Trend = new Trend('Auth_NOK_Trend');
const Auth_NOK_Rate = new Rate('Auth_NOK_Rate');


export const options = {
  vus: 1,
  duration: '10s',
  thresholds: {
    'Auth_NOK_Counter': ['count < 10', ],
  },
};

export default function () {
  const auth_resp = http.post('https://test-api.k6.io/auth/token/login/', {
    username: 'test-user',
    password: 'supersecure',
  });

  check(auth_resp, { 'Auth Status was 200': (r) => r.status == 200 });


  if (auth_resp.status == 200) {
    Auth_OK_Counter.add(1);
    Auth_OK_Rate.add(1);
    Auth_OK_Trend.add(auth_resp.timings.duration);
  } else
  {
    Auth_NOK_Counter.add(1);
    Auth_NOK_Rate.add(1);
    Auth_NOK_Trend.add(auth_resp.timings.duration);
  }

}
