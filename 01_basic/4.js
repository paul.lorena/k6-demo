import http from 'k6/http';

export default function () {
  const url = 'http://httpbin.org/post';
  
  const payload = JSON.stringify({
    id: 11,
    email: 'aaa',
    password: 'bbb',
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  http.post(url, payload, params);
}