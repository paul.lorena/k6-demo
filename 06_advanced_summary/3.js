import { Counter, Trend, Rate } from 'k6/metrics';
import { check } from 'k6';
import { SharedArray } from 'k6/data';

import http from 'k6/http';

const customer_array = new SharedArray('customer_array', function () {
  const f = JSON.parse(open('customers_array.json'));
  return f; 
});



const Auth_OK_Counter = new Counter('Auth_OK_Counter');
const Auth_OK_Trend = new Trend('Auth_OK_Trend');
const Auth_OK_Rate = new Rate('Auth_OK_Rate');
const Auth_NOK_Counter = new Counter('Auth_NOK_Counter');
const Auth_NOK_Trend = new Trend('Auth_NOK_Trend');
const Auth_NOK_Rate = new Rate('Auth_NOK_Rate');

const EndPoint1_OK_Counter = new Counter('EndPoint1_OK_Counter');
const EndPoint1_OK_Trend = new Trend('EndPoint1_OK_Trend');
const EndPoint1_OK_Rate = new Rate('EndPoint1_OK_Rate');
const EndPoint1_NOK_Counter = new Counter('EndPoint1_NOK_Counter');
const EndPoint1_NOK_Trend = new Trend('EndPoint1_NOK_Trend');
const EndPoint1_NOK_Rate = new Rate('EndPoint1_NOK_Rate');

const EndPoint2_OK_Counter = new Counter('EndPoint2_OK_Counter');
const EndPoint2_OK_Trend = new Trend('EndPoint2_OK_Trend');
const EndPoint2_OK_Rate = new Rate('EndPoint2_OK_Rate');
const EndPoint2_NOK_Counter = new Counter('EndPoint2_NOK_Counter');
const EndPoint2_NOK_Trend = new Trend('EndPoint2_NOK_Trend');
const EndPoint2_NOK_Rate = new Rate('EndPoint2_NOK_Rate');


export const options = {
    scenarios: {
        authentication_test: {
        executor: 'constant-vus',
        vus: 50,
        duration: '30s',
        gracefulStop: '0s', 
        tags: { test_type: 'authentication', name:"authentication" }, 
        exec: 'authentication', 
      },
      endpoint1: {
        executor: 'constant-arrival-rate',
        rate: 90,
        timeUnit: '1m', 
        duration: '30s',
        preAllocatedVUs: 10, 
        tags: { test_type: 'endpoint1', name: "endpoint1" }, 
        exec: 'endpoint1', 
      },
      endpoint2: {
        executor: 'ramping-arrival-rate',
        startTime: '5s', 
        startRate: 50,
        timeUnit: '1s', // we start at 50 iterations per second
        stages: [
          { target: 200, duration: '10s' }, 
          { target: 200, duration: '15s' }, 
          { target: 0, duration: '5s' }, 
        ],
        preAllocatedVUs: 50, 
        maxVUs: 100, 
        tags: { test_type: 'endpoint2', name: 'endpoint2' }, 
        exec: 'endpoint2',
      },
    },
    discardResponseBodies: true,
    thresholds: {
      'Auth_NOK_Counter': ['count < 10', ], 
      'http_req_duration{test_type:authentication}': ['p(95)<250', 'p(99)<350'],
      'http_req_duration{test_type:endpoint1}': ['p(95)<250', 'p(99)<350'],
      'http_req_duration{test_type:endpoint2}': ['p(95)<250', 'p(99)<350'],
    },
  };



//Authentication Test
export function authentication () {

  let randomCustomerIndex = Math.floor(Math.random() * customer_array.length)
  let randomCustomer = customer_array[randomCustomerIndex];

  const auth_resp = http.post('https://test-api.k6.io/auth/token/login/', {
    username: randomCustomer.username,
    password: randomCustomer.password,
  });

  check(auth_resp, { 'Auth Status was 200': (r) => r.status == 200 });


  if (auth_resp.status == 200) {
    Auth_OK_Counter.add(1);
    Auth_OK_Rate.add(1);
    Auth_OK_Trend.add(auth_resp.timings.duration);
  } else
  {
    Auth_NOK_Counter.add(1);
    Auth_NOK_Rate.add(1);
    Auth_NOK_Trend.add(auth_resp.timings.duration);
  }

}


//Endpoint 1 Test
export function endpoint1 () {
  const endpoint1_resp = http.post('https://test.k6.io/contacts.php');

  check(endpoint1_resp, { 'Endpoint #1 Status was 200': (r) => r.status == 200 });


  if (endpoint1_resp.status == 200) {
    EndPoint1_OK_Counter.add(1);
    EndPoint1_OK_Rate.add(1);
    EndPoint1_OK_Trend.add(endpoint1_resp.timings.duration);
  } else
  {
    EndPoint1_NOK_Counter.add(1);
    EndPoint1_NOK_Rate.add(1);
    EndPoint1_NOK_Trend.add(endpoint1_resp.timings.duration);
  }
}




//Endpoint 2 Test
export function endpoint2 () {
  const endpoint2_resp = http.post('https://test.k6.io/contacts.php');

  check(endpoint2_resp, { 'Endpoint #2 Status was 200': (r) => r.status == 200 });


  if (endpoint2_resp.status == 200) {
    EndPoint2_OK_Counter.add(1);
    EndPoint2_OK_Rate.add(1);
    EndPoint2_OK_Trend.add(endpoint2_resp.timings.duration);
  } else
  {
    EndPoint2_NOK_Counter.add(1);
    EndPoint2_NOK_Rate.add(1);
    EndPoint2_NOK_Trend.add(endpoint2_resp.timings.duration);
  }
}

