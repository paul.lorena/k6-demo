import http from 'k6/http';
import { SharedArray } from 'k6/data';

const customer_array = new SharedArray('customer_array', function () {
  const f = JSON.parse(open('customers_array.json'));
  return f; 
});




export default function () {
  let url = 'http://httpbin.org/get?id=__ID__&email=__EMAIL__';
  let randomCustomerIndex = Math.floor(Math.random() * customer_array.length)

  url = url.replace("__ID__", customer_array[randomCustomerIndex].id);
  url = url.replace("__EMAIL__", customer_array[randomCustomerIndex].email);

  http.get(url);
}
