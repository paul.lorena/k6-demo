import http from 'k6/http';
const customers = JSON.parse(open("customers.json"));



export default function () {
  let url = 'http://httpbin.org/get?id=__ID__&email=__EMAIL__';
  let randomCustomerIndex = rndItemIndexOnCollection(customers.data);  

  url = url.replace("__ID__", customers.data[randomCustomerIndex].id);
  url = url.replace("__EMAIL__", customers.data[randomCustomerIndex].email);

  http.get(url);
}



/**
 * Get random element from the collection
 */
 export function rndItemIndexOnCollection(collection) {
  let randomItem = Math.floor(Math.random() * collection.length);
  return randomItem;
}
