import http from 'k6/http';

export default function () {
  let url = 'http://httpbin.org/get?id=__ID__&email=__EMAIL__';
  
  const payload = [{
    id: 100,
    email: 'lpaul@eurofins.be',
  }];

  url = url.replace("__ID__", payload[0].id);
  url = url.replace("__EMAIL__", payload[0].email);

  http.get(url);
}