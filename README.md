# k6 Demo - Eurofins 4th February 2022

k6 is a load testing tool written in Go that interpretes Javascript.
It doesn't run in NodeJS nor in a browser.
The current setup is transpiling ES6 to ES5 with babel (coupled with webpack) so that we can profit of all the latest features of ES6 without breaking the intrepreter of k6.
It is used to do load testing and performance monitoring.
k6 is developed by Grafana Labs and the community.

# Load Testing Manifesto

k6 load testing manifesto is the result of having spent years hip deep in the trenches, doing performance- and load testing. Has been created it to be used as guidance, helping you in getting your performance testing on the right track!

Simple testing is better than no testing
Load testing should be goal oriented
Load testing by developers
Developer experience is super important
Load test in a pre-production environment


# Setup and installation k6
Install Visual Studio Code

https://code.visualstudio.com/download


Install NPM

https://nodejs.org/en/


Install k6

https://k6.io/docs/getting-started/installation#windows-msi-installer


Install Git

https://gitforwindows.org/


Clone this project in a local folder in your laptop

git clone https://gitlab.com/paul.lorena/k6-demo.git


**Optional** 

InfluxDB

influxdb2-2.1.1-windows-amd64

https://docs.influxdata.com/influxdb/v2.1/install/?t=Windows


Grafana

Grafana grafana-8.3.4.windows-amd64

https://grafana.com/grafana/download?platform=windows






# Debug

k6 run .\01_basic\1.js   --http-debug=full

# Out
k6 run .\01_basic\1.js  --out json=test.json

or / and

k6 run .\04_advanced\01.js  --out json=test.json --out influxdb=http://localhost:8086/myk6db


[Learn more about k6.](https://k6.io/)

Or contact me : paul.lorena@gmail.com
